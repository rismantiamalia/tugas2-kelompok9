from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class PageCariMahasiswaUnitTest(TestCase):
    def test_page_carimahasiswa_url_is_exist(self):
        response = Client().get('/page_carimahasiswa/')
        self.assertEqual(response.status_code, 200)

    def test_page_carimahasiswa_using_index_func(self):
        found = resolve('/page_carimahasiswa/')
        self.assertEqual(found.func, index)

    def test_cari_mahasiswa_using_teman_template(self):
        response = Client().get('/page_carimahasiswa/')
        self.assertTemplateUsed(response, 'page_carimahasiswa/page_teman.html')
