from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from .api_riwayat import get_matkul

# Create your views here.
response = {}

def index(request):
	response['matkuls'] = get_matkul().json()
	html = 'page_riwayat/riwayat.html'
	return render(request, html, response)

