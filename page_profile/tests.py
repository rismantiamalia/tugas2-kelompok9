from django.test import TestCase
from django.test import Client

# Create your tests here.
class PageRiwayatUnitTest(TestCase):
    def test_page_profile_url_is_exist(self):
    	response = Client().get('/page_profile/')
    	self.assertEqual(response.status_code, 200)

    # def test_edit_profile_when_user_is_logged_in_and_otherwise(self):
    #     #not logged in
    #     response = self.client.get('/page_profile/edit_profile/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertFalse(response.context['login'])
    #
    #     #logged in
    #     response = self.client.post('/page_login/custom_auth/login/', {'username': self.username, 'password': self.password})
    #     self.assertEqual(response.status_code, 302)
    #     response = self.client.get('/page_profile/edit_profile/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTrue(response.context['login'])
    #     response = self.client.get('/page_profile/edit_profile/', {'kode_identitas':'1606918282'})
    #     self.assertEqual(response.status_code, 200)
