from django.shortcuts import render

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from page_status.tambahan import get_data_user
response = {}
# Create your views here.
def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('page_status:index'))
    else:
        response['author'] = get_data_user(request, 'user_login')
        html = 'page_login/page_login.html'
        return render(request, html, response)
