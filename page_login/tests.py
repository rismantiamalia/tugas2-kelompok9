from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class PageLoginUnitTest(TestCase):
    def test_page_login_url_is_exist(self):
        response = Client().get('/page_login/')
        self.assertEqual(response.status_code, 200)

    def test_page_login_when_user_is_logged_in_or_not(self):
        #not logged in, render login template
        response = self.client.get('/page_login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('page_login/page_login.html')

        #logged in, redirect to profile page
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/page_login/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/page_login/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('/page_status/page_status.html')
        
    def test_direct_access_to_profile_url(self):
        #not logged in, redirect to login page
        response = self.client.get('/page_login/')
        self.assertEqual(response.status_code, 200)

        #logged in, render profile template
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/page_login/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/page_status/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/page_login/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/page_login/custom_auth/logout/')
        html_response = self.client.get('/page_login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        
    def test_invalid_sso_raise_exception(self):
        username = "username"
        password = "password"
        get_access_token(username, password)
        response = self.client.get('/page_login/')
        self.assertEqual(response.status_code, 200)

