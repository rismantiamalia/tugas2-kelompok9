#Tugas Pemrograman 02 Perancangan dan Pemrograman Web

Pipeline status: ![pipeline status](https://gitlab.com/rismantiamalia/tugas2-kelompok9/badges/master/pipeline.svg)

Coverage report: ![coverage report](https://gitlab.com/rismantiamalia/tugas2-kelompok9/badges/master/coverage.svg)

Link: tugasduappw.herokuapp.com

Creator:
1. Kelly William (1606876784)
   Developer Halaman Login dan Status
2. Nurul Faza Saffanah (1606886186)
   Developer Halaman Cari Mahasiswa
3. Rismanti Amalia N (1606918282)
   Developer Halaman Profile
4. Tyagita Larasati (1606918616)
   Developer Halaman Riwayat

Semoga karya kami dapat dinikmati oleh Anda (:
