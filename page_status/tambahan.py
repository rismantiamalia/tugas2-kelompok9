from .models import *

def check_status_in_database(request, activity):
    is_exist = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    count_status = Update.objects.filter(pengguna=pengguna, activity=activity).count()
    if count_status > 0 :
        is_exist = True

    return is_exist

def check_movie_in_session(request, activity):
    is_exist = False
    ssn_key = request.session.keys()
    if 'activity' in ssn_key:
        status = request.session['status']
        if activity in status:
            is_exist = True

    return is_exist

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna

def add_status_to_session(request, suatuTulisan):
    ssn_key = request.session.keys()
    if not 'status' in ssn_key:
        request.session['status'] = suatuTulisan
    else:
        activity = request.session['status']
        # check apakah di session sudah ada key yang sama
        if suatuTulisan not in activity:
            activity.append(suatuTulisan)
            request.session['status'] = activity

def get_status_from_session(request):
    resp = []
    ssn_key = request.session.keys()
    if 'status' in ssn_key:
        resp = request.session['status']
    return resp
    
def add_status_to_database(request, suatuTulisan):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    statusku = Update()
    statusku.activity = suatuTulisan
    statusku.pengguna = pengguna
    statusku.save()
    
def save_status_to_database(pengguna, list_status):
    for status in list_status:
        if not (Update.objects.filter(pengguna = pengguna, activity=activity).count()) > 0:
            new_status = Update()
            new_status.pengguna = pengguna
            new_status.activity = activity
            new_status.save()
            
def get_my_status_from_database(request):
    resp = []
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    items = Update.objects.filter(pengguna=pengguna)
    for item in items:
        print(items.length)
        resp.append(item.activity)
    return resp




