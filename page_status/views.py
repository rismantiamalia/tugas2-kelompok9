from django.shortcuts import render

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from page_riwayat.api_riwayat import get_matkul
from page_profile.views import index
from page_profile.models import User
from .models import Update, Pengguna
from .forms import Status_Form
from .tambahan import *

response = {}

# Create your views here.
def index(request):
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('page_login:index'))
    else:
        set_data_for_session(response , request)
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)

        list_status = get_status_from_session(request)
        save_status_to_database(pengguna, list_status)
        
        pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        response['status'] = pengguna.update_set.all().order_by('-id')
        response['jstat'] = pengguna.update_set.count()

        stat = pengguna.update_set.all().order_by('-id')
        if(len(stat) > 0):
            message = stat[0]
            newMessage = message.activity
            time = message.created_at
        else:
            newMessage = 'message not available'
            
        response['lastStatus']= newMessage
        response['matkuls'] = get_matkul().json()
        users = User.objects.all()
        response['users'] = users
        npm = request.session['kode_identitas']
        user = (User.objects.filter(kode_identitas=npm)
                             .values('firstName', 'lastName','imageUrl',
                             'email','profileUrl','kode_identitas','angkatan'))
        if(user):
            response['user'] = user[0]
        html = 'page_status/page_status.html'
        return render(request, html, response)

def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')
        
def set_data_for_session(response, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['update_form'] = Status_Form

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        suatuTulisan = request.POST['activity']
        if get_data_user(request, 'user_login'):
            print("TO DB")
            is_in_db = check_status_in_database(request, suatuTulisan)
            if not is_in_db:
                add_status_to_database(request, suatuTulisan)
        else:
            print("TO SESSION")
            is_in_ssn = check_status_in_session(request, suatuTulisan)
            if not is_in_ssn:
                add_status_to_session(request, suatuTulisan)

    return HttpResponseRedirect(reverse('page_status:index'))
        
def list_status(request):
    status = []
    if get_data_user(request, 'user_login'):
        status = get_my_status_from_database(request)
    else:
        status = get_my_status_from_session(request)

    response['status'] = status
    html = 'page_status/status.html'
    return render(request, html, response)
