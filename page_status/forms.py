from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs = {
        'type': 'text',
        'cols': 80,
        'rows': 10,
        'class': 'update-form-textarea',
        'placeholder':'Whats on your mind?'
    }

    activity = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
