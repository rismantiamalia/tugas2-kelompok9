from django.test import TestCase
from django.test import Client

# Create your tests here.
class PageStatusUnitTest(TestCase):
    def test_page_status_url_is_exist(self):
        response = Client().get('/page_status/')
        self.assertEqual(response.status_code, 302)
