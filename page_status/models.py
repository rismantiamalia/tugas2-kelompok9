from __future__ import unicode_literals
from django.db import models

class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField('Nama', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    
class Update(models.Model):
    pengguna = models.ForeignKey(Pengguna)
    created_at = models.DateTimeField(auto_now_add=True)
    activity = models.TextField()


